var MenuBJC = function(node) {

        var menuBJC = $(
		'<nav class="nav-menu navbar" role="navigation">'+
		    '<div class="container">'+
		        '<div class="navbar-header">'+
		            '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">'+
		                '<span class="sr-only">Toggle navigation</span>'+
		                '<span class="icon-bar"></span>'+
		                '<span class="icon-bar"></span>'+
		                '<span class="icon-bar"></span>'+
		            '</button>'+
		        '</div>'+
		        '<div class="collapse navbar-collapse" id="navbar-collapse-1">'+
		            '<ul class="nav navbar-nav">'+
		                '<li><a id="home-selected" href="index.html" class="main-menu-home-icon i18n" data-i18n="[html]menu.bjc.home"></a></li>'+
		                '<li><a id="bible-selected" href="lire.html" class="i18n" data-i18n="[html]menu.bjc.bible"></a></li>'+
		                '<li><a id="download-selected" href="telecharger.html" class="i18n" data-i18n="[html]menu.bjc.download"></a></li>'+
		                '<li><a id="donate-selected" href="soutenir.html" class="i18n" data-i18n="[html]menu.bjc.donate"></a></li>'+
		                '<li><a id="contact-selected" href="contact.html" class="i18n" data-i18n="[html]menu.bjc.contact"></a></li>'+
		            '</ul>'+
		        '</div>'+
		    '</div>'+
		'</nav>'
		).appendTo(node)
        return menuBJC;
};
sofia.menuComponents.push('MenuBJC');
